package hyl.android.redux;

import android.util.Log;

import java.util.List;

public abstract class Subscription {

    public abstract void unSubscribe();

    public static Subscription create(final List<Subscriber> subscribers, final Subscriber subscriber) {
        return new Subscription() {
            @Override
            public void unSubscribe() {
                boolean remove = subscribers.remove(subscriber);
                Log.i("Subscription", "Subscription remove " + subscriber + " " + remove);
            }
        };
    }
}