package hyl.android.redux;


import hyl.android.redux.action.Action;
import hyl.android.redux.reducer.Reducer;
import hyl.android.redux.state.State;

public abstract class Store<A extends Action, S extends State> {

    public static <A extends Action, S extends State> CoreStore<A, S> create(S initialState, Reducer<A, S> reducer, Middleware<A, S>... middlewares) {
        return new CoreStore<>(initialState, reducer, middlewares);
    }

    public abstract void subscribe(Subscriber subscriber);

    public abstract void unsubscribe(Subscriber subscriber);

    public abstract S getState();

    public abstract void dispatch(A action);
}