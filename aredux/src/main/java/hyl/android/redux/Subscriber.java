package hyl.android.redux;

public interface Subscriber {
    void onNotifyChangeState();
}