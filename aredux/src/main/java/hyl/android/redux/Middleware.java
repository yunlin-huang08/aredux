package hyl.android.redux;


import hyl.android.redux.action.Action;
import hyl.android.redux.state.State;

public interface Middleware<A extends Action, S extends State> {
    void dispatch(Store<A, S> store, A action);
}