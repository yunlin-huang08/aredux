package hyl.android.redux.action;


import hyl.android.redux.Store;
import hyl.android.redux.Subscriber;
import hyl.android.redux.state.State;

/**
 * User: HuangYunLin(283857754@qq.com)
 * Date: 2016-04-21
 * Time: 14:51
 * FIXME
 */
public abstract class ActionCreator {

    private final Store store;

    public ActionCreator() {
        this.store = createStore();
    }

    protected abstract Store createStore();

    public abstract State getCurrentState();

    public void subscribe(Subscriber subscriber) {
        store.subscribe(subscriber);
    }

    public void unsubscribe(Subscriber subscriber) {
        store.unsubscribe(subscriber);
    }
}
