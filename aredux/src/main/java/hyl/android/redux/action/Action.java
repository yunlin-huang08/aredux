package hyl.android.redux.action;

public class Action<V> {

    public int type;
    public V   value;

    public Action(int type) {
        this(type, null);
    }

    public Action(int type, V value) {
        this.type = type;
        this.value = value;
    }
}