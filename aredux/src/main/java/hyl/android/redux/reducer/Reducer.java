package hyl.android.redux.reducer;


import hyl.android.redux.action.Action;
import hyl.android.redux.state.State;

public interface Reducer<A extends Action, S extends State> {
    S call(A action, S state);
}