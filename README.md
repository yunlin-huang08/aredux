# Android Redux

在了解js中redux模式后,想在Android开发中尝试使用redux模式,简单的封装了一个Android版的redux库

##### 使用方式:
创建一个 AppActionCreator

```java
public class AppActionCreator extends ActionCreator {

    private Store<Action, AppState> store;

    private static AppActionCreator appActionCreator = new AppActionCreator();

    public static AppActionCreator getAppActionCreator() {
        return appActionCreator;
    }

    @Override
    protected Store createStore() {
        this.store = Store.create(AppState.create(), new XReducer());
        return store;
    }

    @Override
    public AppState getCurrentState() {
        return store.getState();
    }

    public void dispatch(Action action) {
        store.dispatch(action);
    }
}
```
创建一个reducer

```java
public class XReducer extends BaseReducer<Action, AppState> {

    @ActionType(value = Action.XXXXX)
    public AppState onGetOrdersAction(OrderAction<ApiResponse<Order>> action, AppState state) {
        //处理数据后返回更新后的state
        return state;
    }
}
```
创建一个action

```java
view.setOnclickListener{
    appActionCreator.dispatch(new Action(Action.XXXXX,object));
}
```
最后在activity中订阅Subscriber

```java
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(null);
        AppActionCreator.getAppActionCreator().subscribe(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppActionCreator.getAppActionCreator().unsubscribe(this);
    }
    
     @Override
    public void onNotifyChangeState() {
        AppState appState = AppActionCreator.getAppActionCreator().getCurrentState();

        if (appState.type == Action.XXXXX) {
            //获得当前的state,更新UI
        }

    }


```




